﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaTeTi___IA___Forms
{
    public partial class EasyMode : Form
    {
        public class Casillero
        {
            public Casillero()
            {
                valor = ' ';
                ocupado = false;
            }

            public char valor;
            public bool ocupado;
        }

        public class Tablero
        {
            public Casillero[,] valores;

            public Tablero(int f, int c)
            {
                valores = new Casillero[f, c];
            }

            public void BorrarTablero()
            {
                for (int i = 0; i < valores.GetLength(0); i++)
                {
                    for (int j = 0; j < valores.GetLength(1); j++)
                    {
                        valores[i, j] = new Casillero();
                    }
                }
            }

            public bool HayCasilleroDisponibles()
            {
                bool state = false;

                for (int i = 0; i < valores.GetLength(0); i++)
                {
                    for (int j = 0; j < valores.GetLength(1); j++)
                    {
                        if (!valores[i, j].ocupado)
                        {
                            state = true;
                        }
                    }
                }

                return state;
            }
        }

        public class Jugador
        {
            public Jugador()
            {

            }

            public Jugador(string n, char f)
            {
                nombre = n;
                ficha = f;
            }

            public string nombre;
            public char ficha;
        }

        public class IA : Jugador
        {
            public char[,] tableroInterno = new char[8, 4];

            public int[] objetivo = new int[2];

            public bool encontreObjetivo = false;

            public void ElegirCasillero(Tablero tablero)
            {
                if (!partida.gameOver)
                {
                    Random rnd = new Random();
                    bool turno = true;
                    while (turno)
                    {
                        int fila = rnd.Next(0, 3);
                        int col = rnd.Next(0, 3);
                        if (!tablero.valores[fila, col].ocupado)
                        {
                            MarcarCasillero(fila, col, ficha);
                            GraficarTablero(picMatrix, tablero);
                            turno = false;
                        }
                    }
                }
            }
        }

        public class Partida
        {
            public Partida()
            {
                jugador = new Jugador();
                bot = new IA();
                tablero = new Tablero(3, 3);
            }

            public Jugador jugador;
            public IA bot;

            public char fichaGanadora;

            public bool gameOver;

            public Tablero tablero;

            public void InicializarPartida()
            {
                jugador = new Jugador();
                bot = new IA();

                jugador.ficha = 'X';
                bot.ficha = 'O';
                fichaGanadora = ' ';

                gameOver = false;
                tablero.BorrarTablero();
            }

        }

        public static Partida partida = new Partida();

        public static PictureBox[,] picMatrix = new PictureBox[3, 3];

        public EasyMode()
        {
            InitializeComponent();

            InicializarPicArray(picMatrix);

            partida.InicializarPartida();
        }

        private void InicializarPicArray(PictureBox[,] picMtx)
        {
            picMtx[0, 0] = picBox00;
            picMtx[0, 1] = picBox01;
            picMtx[0, 2] = picBox02;
            picMtx[1, 0] = picBox10;
            picMtx[1, 1] = picBox11;
            picMtx[1, 2] = picBox12;
            picMtx[2, 0] = picBox20;
            picMtx[2, 1] = picBox21;
            picMtx[2, 2] = picBox22;
        }

        public static void GraficarTablero(PictureBox[,] picMtx, Tablero tablero)
        {
            for (int i = 0; i < picMtx.GetLength(0); i++)
            {
                for (int j = 0; j < picMtx.GetLength(1); j++)
                {
                    switch (tablero.valores[i, j].valor)
                    {
                        case 'X':
                            picMtx[i, j].Image = Properties.Resources.Cross;
                            picMtx[i, j].Enabled = false;
                            break;
                        case 'O':
                            picMtx[i, j].Image = Properties.Resources.Circle;
                            picMtx[i, j].Enabled = false;
                            break;
                        case ' ':
                            picMtx[i, j].Image = null;
                            picMtx[i, j].Enabled = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public static void MarcarCasillero(int f, int c, char ficha)
        {
            partida.tablero.valores[f, c].valor = ficha;
            partida.tablero.valores[f, c].ocupado = true;
            GraficarTablero(picMatrix, partida.tablero);
            ChequearVictoria(partida.tablero);
        }

        public static void ChequearVictoria(Tablero tablero)
        {

            for (int i = 0; i < 3; i++)
            {
                if (!partida.gameOver)
                {
                    if (tablero.valores[0, i].valor != ' ' &&
                    tablero.valores[0, i].valor == tablero.valores[1, i].valor &&
                    tablero.valores[0, i].valor == tablero.valores[2, i].valor)
                    {
                        partida.gameOver = true;
                        partida.fichaGanadora = tablero.valores[0, i].valor;
                        i = tablero.valores.GetLength(0);
                    }
                }

                if (!partida.gameOver)
                {
                    if (tablero.valores[i, 0].valor != ' ' &&
                    tablero.valores[i, 0].valor == tablero.valores[i, 1].valor &&
                    tablero.valores[i, 0].valor == tablero.valores[i, 2].valor)
                    {
                        partida.gameOver = true;
                        partida.fichaGanadora = tablero.valores[i, 0].valor;
                        i = tablero.valores.GetLength(0);
                    }
                }
            }

            if (!partida.gameOver)
            {
                if (tablero.valores[0, 0].valor != ' ' &&
               tablero.valores[0, 0].valor == tablero.valores[1, 1].valor &&
               tablero.valores[0, 0].valor == tablero.valores[2, 2].valor)
                {
                    partida.gameOver = true;
                    partida.fichaGanadora = tablero.valores[1, 1].valor;
                }
                else if (tablero.valores[0, 2].valor != ' ' &&
                         tablero.valores[0, 2].valor == tablero.valores[1, 1].valor &&
                         tablero.valores[0, 2].valor == tablero.valores[2, 0].valor)
                {
                    partida.gameOver = true;
                    partida.fichaGanadora = tablero.valores[1, 1].valor;
                }
            }

            if (!partida.tablero.HayCasilleroDisponibles() && partida.fichaGanadora == ' ')
            {
                partida.gameOver = true;
                MessageBox.Show("Empate");
            }

            if (partida.gameOver)
            {
                FinalizarPartida();
            }
        }

        public static void FinalizarPartida()
        {
            if (partida.jugador.ficha == partida.fichaGanadora)
            {
                MessageBox.Show("Ganaste!");
            }
            else if (partida.bot.ficha == partida.fichaGanadora)
            {
                MessageBox.Show("Perdiste!");
            }

            foreach (PictureBox pcbox in picMatrix)
            {
                pcbox.Enabled = false;
            }
        }


        #region PicBoxes

        private void picBox00_Click(object sender, EventArgs e)
        {
            MarcarCasillero(0, 0, partida.jugador.ficha);
            if (partida.tablero.HayCasilleroDisponibles())
            {
                partida.bot.ElegirCasillero(partida.tablero);
            }
        }

        private void picBox01_Click(object sender, EventArgs e)
        {
            MarcarCasillero(0, 1, partida.jugador.ficha);
            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox02_Click(object sender, EventArgs e)
        {
            MarcarCasillero(0, 2, partida.jugador.ficha);
            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox10_Click(object sender, EventArgs e)
        {
            MarcarCasillero(1, 0, partida.jugador.ficha);

            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox11_Click(object sender, EventArgs e)
        {
            MarcarCasillero(1, 1, partida.jugador.ficha);

            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox12_Click(object sender, EventArgs e)
        {
            MarcarCasillero(1, 2, partida.jugador.ficha);

            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox20_Click(object sender, EventArgs e)
        {
            MarcarCasillero(2, 0, partida.jugador.ficha);

            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox21_Click(object sender, EventArgs e)
        {
            MarcarCasillero(2, 1, partida.jugador.ficha);

            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        private void picBox22_Click(object sender, EventArgs e)
        {
            MarcarCasillero(2, 2, partida.jugador.ficha);

            if (partida.tablero.HayCasilleroDisponibles())
            {

                partida.bot.ElegirCasillero(partida.tablero);

            }
        }

        #endregion

        private void btnReset_Click(object sender, EventArgs e)
        {
            partida.InicializarPartida();
            GraficarTablero(picMatrix, partida.tablero);
        }

        private void picBoxCross_Click(object sender, EventArgs e)
        {
            partida.InicializarPartida();
        }

        private void picBoxCircle_Click(object sender, EventArgs e)
        {
            partida.InicializarPartida();
            partida.jugador.ficha = 'O';
            partida.bot.ficha = 'X';

            partida.bot.ElegirCasillero(partida.tablero);
        }
    }
}
