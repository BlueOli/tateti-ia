﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaTeTi___IA___Forms
{
    public partial class HardMode : Form
    {
        public class Casillero
        {
            public Casillero()
            {
                valor = ' ';
                ocupado = false;
            }

            public char valor;
            public bool ocupado;

            public void Copiar(Casillero casObjetivo)
            {
                casObjetivo.valor = this.valor;
                casObjetivo.ocupado = this.ocupado;
            }
        }

        public class Tablero
        {
            public Casillero[,] valores;

            public int puntaje;

            public string codigo;

            public Tablero[] tableros;

            public bool tengoHijosDefinidos = false;

            public int cantidadDisponible = 0;

            public int cantidadX = 0;

            public int cantidadO = 0;

            public Tablero(int f, int c)
            {
                puntaje = 10;

                valores = new Casillero[f, c];
                for(int i = 0; i < f; i++)
                {
                    for(int j = 0; j<c; j++)
                    {
                        valores[i, j] = new Casillero();
                    }
                }
            }

            public void BorrarTablero()
            {
                for (int i = 0; i < valores.GetLength(0); i++)
                {
                    for (int j = 0; j < valores.GetLength(1); j++)
                    {
                        valores[i, j] = new Casillero();
                    }
                }
            }

            public bool HayCasilleroDisponibles()
            {
                cantidadDisponible = 0;
                cantidadO = 0;
                cantidadX = 0;
                bool state = false;

                for (int i = 0; i < valores.GetLength(0); i++)
                {
                    for (int j = 0; j < valores.GetLength(1); j++)
                    {
                        if (!valores[i, j].ocupado)
                        {
                            if (!state) state = true;
                            cantidadDisponible++;
                        }
                        else
                        {
                            if(valores[i, j].valor == 'X')
                            {
                                cantidadX++;
                            }
                            else if (valores[i, j].valor == 'O')
                            {
                                cantidadO++;
                            }
                        }
                    }
                }

                return state;
            }

            public void Copiar(Tablero tabObjetivo)
            {
                tabObjetivo.cantidadDisponible = this.cantidadDisponible;
                tabObjetivo.cantidadO = this.cantidadO;
                tabObjetivo.cantidadX = this.cantidadX;
                tabObjetivo.puntaje = this.puntaje;

                for(int i = 0; i < 3; i++)
                {
                    for(int j = 0; j<3; j++)
                    {
                        valores[i, j].Copiar(tabObjetivo.valores[i, j]);
                    }
                }
            }
        }

        public class Jugador
        {
            public Jugador()
            {

            }

            public Jugador(string n, char f)
            {
                nombre = n;
                ficha = f;
            }

            public string nombre;
            public char ficha;
        }

        public class IA : Jugador
        {
            public Tablero tableroInterno = new Tablero(3,3);

            public int[] objetivo = new int[2];

            public int indiceTableroDeseado = 10;

            int mejorPuntaje = -20;

            bool tengoChance = false;

            public bool encontreObjetivo = false;

            public void ElegirCasillero(Tablero tablero)
            {
                int contador = 0;
                if (!partida.gameOver)
                {
                    if(!tengoChance)
                    {
                        Random rnd = new Random();
                        bool turno = true;
                        while (turno)
                        {
                            int fila = rnd.Next(0, 3);
                            int col = rnd.Next(0, 3);
                            if (!tablero.valores[fila, col].ocupado)
                            {
                                MarcarCasillero(fila, col, ficha);
                                GraficarTablero(picMatrix, tablero);
                                turno = false;
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                if (!tablero.valores[i, j].ocupado)
                                {
                                    if (contador < indiceTableroDeseado)
                                    {
                                        contador++;
                                    }
                                    else
                                    {
                                        MarcarCasillero(i, j, ficha);
                                        i = 3;
                                        j = 3;
                                    }
                                }
                            }
                        }
                    }   
                }
            }

            public void GuardarTablero(Tablero tablero)
            {
                tablero.Copiar(tableroInterno);
            }

            public void DefinirTablerosHijos(Tablero tablero, int recur)
            {
                if (tablero.HayCasilleroDisponibles())
                {
                    if (!tablero.tengoHijosDefinidos)
                    {
                        tablero.tableros = new Tablero[tablero.cantidadDisponible];
                        tablero.tengoHijosDefinidos = true;
                    }
                    
                    for(int i = 0; i < tablero.cantidadDisponible; i++)
                    {
                        tablero.tableros[i] = new Tablero(3, 3);
                        tablero.Copiar(tablero.tableros[i]);
                        
                        SimularSiguienteJugada(tablero.tableros[i], i);
                        tablero.tableros[i].codigo = recur + ": " + i;
                        char fichaGanadora = ChequearFichaGanadora(tablero.tableros[i]);
                        if (fichaGanadora != ' ')
                        {
                            if (fichaGanadora == '?')
                            {
                                tablero.tableros[i].puntaje = 0;
                            }
                            else if (fichaGanadora == ficha)
                            {
                                tablero.tableros[i].puntaje = 10 - recur;
                            }
                            else
                            {
                                 tablero.tableros[i].puntaje = recur - 10;
                            }
                        }
                        else
                        {
                            DefinirTablerosHijos(tablero.tableros[i], recur + 1);
                        }
                    }
                    
                    foreach(Tablero tab in tablero.tableros)
                    {
                        if (tablero.puntaje > tab.puntaje) tablero.puntaje = tab.puntaje;                    
                        tablero.tengoHijosDefinidos = false;
                    }
                }
            }

            public void SimularSiguienteJugada(Tablero tablero, int skip)
            {
                char ficha;
                int contador = 0;

                if(tablero.cantidadX == tablero.cantidadO)
                {
                    ficha = 'X';
                }
                else
                {
                    ficha = 'O';
                }

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (!tablero.valores[i, j].ocupado)
                        {
                            if (contador < skip)
                            {
                                contador++;
                            }
                            else
                            {
                                tablero.valores[i, j].valor = ficha;
                                tablero.valores[i, j].ocupado = true;
                                i = 3;
                                j = 3;
                            }
                        }
                    }
                }
            }

            public void PuntuarTablero()
            {
                GuardarTablero(partida.tablero);
                DefinirTablerosHijos(tableroInterno, 0);
                ElegirMejorMovimiento();
            }

            public void ElegirMejorMovimiento()
            {
                indiceTableroDeseado = 10;
                mejorPuntaje = -20;

                int index = 0;
                foreach(Tablero tab in tableroInterno.tableros)
                {
                    if (mejorPuntaje < tableroInterno.tableros[index].puntaje)
                    {
                        mejorPuntaje = tableroInterno.tableros[index].puntaje;
                        indiceTableroDeseado = index;
                    }
                    index++;
                }

                tengoChance = false;
                foreach (Tablero tab in tableroInterno.tableros)
                {
                    if (mejorPuntaje != tab.puntaje)
                    {
                        tengoChance = true;
                    }
                }

                    ElegirCasillero(partida.tablero);
            }

        }

        public class Partida
        {
            public Partida()
            {
                jugador = new Jugador();
                bot = new IA();
                tablero = new Tablero(3, 3);
            }

            public Jugador jugador;
            public IA bot;

            public char fichaGanadora;

            public bool gameOver;

            public Tablero tablero;

            public void InicializarPartida()
            {
                jugador = new Jugador();
                bot = new IA();

                jugador.ficha = 'X';
                bot.ficha = 'O';
                fichaGanadora = ' ';

                gameOver = false;
                tablero.BorrarTablero();
            }

        }

        public static Partida partida = new Partida();

        public static PictureBox[,] picMatrix = new PictureBox[3, 3];

        public HardMode()
        {
            InitializeComponent();

            InicializarPicArray(picMatrix);

            partida.InicializarPartida();
        }

        private void InicializarPicArray(PictureBox[,] picMtx)
        {
            picMtx[0, 0] = picBox00;
            picMtx[0, 1] = picBox01;
            picMtx[0, 2] = picBox02;
            picMtx[1, 0] = picBox10;
            picMtx[1, 1] = picBox11;
            picMtx[1, 2] = picBox12;
            picMtx[2, 0] = picBox20;
            picMtx[2, 1] = picBox21;
            picMtx[2, 2] = picBox22;
        }

        public static void GraficarTablero(PictureBox[,] picMtx, Tablero tablero)
        {
            for (int i = 0; i < picMtx.GetLength(0); i++)
            {
                for (int j = 0; j < picMtx.GetLength(1); j++)
                {
                    switch (tablero.valores[i, j].valor)
                    {
                        case 'X':
                            picMtx[i, j].Image = Properties.Resources.Cross;
                            picMtx[i, j].Enabled = false;
                            break;
                        case 'O':
                            picMtx[i, j].Image = Properties.Resources.Circle;
                            picMtx[i, j].Enabled = false;
                            break;
                        case ' ':
                            picMtx[i, j].Image = null;
                            picMtx[i, j].Enabled = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public static void MarcarCasillero(int f, int c, char ficha)
        {
            partida.tablero.valores[f, c].valor = ficha;
            partida.tablero.valores[f, c].ocupado = true;
            GraficarTablero(picMatrix, partida.tablero);
            char fichaGanadora = ChequearFichaGanadora(partida.tablero);
            if (fichaGanadora != ' ')
            {
                FinalizarPartida(fichaGanadora);
            }
        }

        public static char ChequearFichaGanadora(Tablero tablero)
        {
            char fichaGanadora = ' ';
            bool encontreFicha = false;

            for (int i = 0; i < 3; i++)
            {
                if (!encontreFicha)
                {
                    if (tablero.valores[0, i].valor != ' ' &&
                    tablero.valores[0, i].valor == tablero.valores[1, i].valor &&
                    tablero.valores[0, i].valor == tablero.valores[2, i].valor)
                    {
                        fichaGanadora = tablero.valores[0, i].valor;
                        encontreFicha = true;
                        i = tablero.valores.GetLength(0);
                    }
                }

                if (!encontreFicha)
                {
                    if (tablero.valores[i, 0].valor != ' ' &&
                    tablero.valores[i, 0].valor == tablero.valores[i, 1].valor &&
                    tablero.valores[i, 0].valor == tablero.valores[i, 2].valor)
                    {
                        fichaGanadora = tablero.valores[i, 0].valor;
                        encontreFicha = true;
                        i = tablero.valores.GetLength(0);
                    }
                }
            }

            if (!encontreFicha)
            {
                if (tablero.valores[0, 0].valor != ' ' &&
               tablero.valores[0, 0].valor == tablero.valores[1, 1].valor &&
               tablero.valores[0, 0].valor == tablero.valores[2, 2].valor)
                {
                    fichaGanadora = tablero.valores[1, 1].valor;
                    encontreFicha = true;
                }
                else if (tablero.valores[0, 2].valor != ' ' &&
                         tablero.valores[0, 2].valor == tablero.valores[1, 1].valor &&
                         tablero.valores[0, 2].valor == tablero.valores[2, 0].valor)
                {
                    fichaGanadora = tablero.valores[1, 1].valor;
                    encontreFicha = true;
                }
            }

            if (!partida.tablero.HayCasilleroDisponibles() && fichaGanadora == ' ')
            {
                return '?';
            }
            else
            {
                return fichaGanadora;
            }
        }

        public static void FinalizarPartida(char fichaGanadora)
        {
            if (partida.jugador.ficha == fichaGanadora)
            {
                MessageBox.Show("Ganaste!");
            }
            else if (partida.bot.ficha == fichaGanadora)
            {
                MessageBox.Show("Perdiste!");
            }

            foreach (PictureBox pcbox in picMatrix)
            {
                pcbox.Enabled = false;
            }
        }


        #region PicBoxes

        private void picBox00_Click(object sender, EventArgs e)
        {
            MarcarCasillero(0, 0, partida.jugador.ficha);
        }

        private void picBox01_Click(object sender, EventArgs e)
        {
            MarcarCasillero(0, 1, partida.jugador.ficha);
        }

        private void picBox02_Click(object sender, EventArgs e)
        {
            MarcarCasillero(0, 2, partida.jugador.ficha);
        }

        private void picBox10_Click(object sender, EventArgs e)
        {
            MarcarCasillero(1, 0, partida.jugador.ficha);
        }

        private void picBox11_Click(object sender, EventArgs e)
        {
            MarcarCasillero(1, 1, partida.jugador.ficha);
        }

        private void picBox12_Click(object sender, EventArgs e)
        {
            MarcarCasillero(1, 2, partida.jugador.ficha);
        }

        private void picBox20_Click(object sender, EventArgs e)
        {
            MarcarCasillero(2, 0, partida.jugador.ficha);
        }

        private void picBox21_Click(object sender, EventArgs e)
        {
            MarcarCasillero(2, 1, partida.jugador.ficha);
        }

        private void picBox22_Click(object sender, EventArgs e)
        {
            MarcarCasillero(2, 2, partida.jugador.ficha);
        }

        #endregion

        private void btnReset_Click(object sender, EventArgs e)
        {
            partida.InicializarPartida();
            GraficarTablero(picMatrix, partida.tablero);
        }

        private void picBoxCross_Click(object sender, EventArgs e)
        {
            partida.InicializarPartida();
        }

        private void picBoxCircle_Click(object sender, EventArgs e)
        {
            partida.InicializarPartida();
            partida.jugador.ficha = 'O';
            partida.bot.ficha = 'X';

            lstboxTablero.Items.Add("----- NUEVA JUGADA -----");
            partida.bot.PuntuarTablero();
            foreach (Tablero tab in partida.bot.tableroInterno.tableros)
            {
                MostrarTablero(tab, lstboxTablero);
            }
        }

        private void BuscarTablerosHijos(Tablero tablero, ListBox lstbox)
        {
            if(tablero.tableros != null)
            {
                foreach(Tablero tab in tablero.tableros)
                {
                    BuscarTablerosHijos(tab, lstbox);
                }
            }
            MostrarTablero(tablero, lstbox);
        }

        private void btnPuntaje_Click(object sender, EventArgs e)
        {
            Tablero tmpTablero = partida.tablero;
            lstBoxPuntajes.Items.Add(partida.tablero.puntaje.ToString());
            partida.tablero = tmpTablero;
        }

        private void btnTablero_Click(object sender, EventArgs e)
        {
            lstboxTablero.Items.Add("----- NUEVA JUGADA -----");
            partida.bot.PuntuarTablero();
            foreach (Tablero tab in partida.bot.tableroInterno.tableros)
            {
                MostrarTablero(tab, lstboxTablero);
            }
        }

        public void MostrarTablero(Tablero tablero, ListBox lstboxTablero)
        {
            lstboxTablero.Items.Add("Codigo:" + tablero.codigo);
            lstboxTablero.Items.Add("Puntaje: " + tablero.puntaje.ToString());

            for (int i = 0; i < 3; i++)
            {
                lstboxTablero.Items.Add(tablero.valores[i, 0].valor.ToString() + "  " +
                                        tablero.valores[i, 1].valor.ToString() + "  " +
                                        tablero.valores[i, 2].valor.ToString());
            }

            lstboxTablero.Items.Add("----------");
        }
    }
}
