﻿
namespace TaTeTi___IA___Forms
{
    partial class HardMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HardMode));
            this.picBoxCircle = new System.Windows.Forms.PictureBox();
            this.picBoxCross = new System.Windows.Forms.PictureBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.picBox22 = new System.Windows.Forms.PictureBox();
            this.picBox21 = new System.Windows.Forms.PictureBox();
            this.picBox20 = new System.Windows.Forms.PictureBox();
            this.picBox12 = new System.Windows.Forms.PictureBox();
            this.picBox11 = new System.Windows.Forms.PictureBox();
            this.picBox10 = new System.Windows.Forms.PictureBox();
            this.picBox02 = new System.Windows.Forms.PictureBox();
            this.picBox01 = new System.Windows.Forms.PictureBox();
            this.picBox00 = new System.Windows.Forms.PictureBox();
            this.btnPuntaje = new System.Windows.Forms.Button();
            this.lstBoxPuntajes = new System.Windows.Forms.ListBox();
            this.lstboxTablero = new System.Windows.Forms.ListBox();
            this.btnTablero = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCircle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCross)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox00)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxCircle
            // 
            this.picBoxCircle.Image = global::TaTeTi___IA___Forms.Properties.Resources.Circle;
            this.picBoxCircle.Location = new System.Drawing.Point(71, 16);
            this.picBoxCircle.Name = "picBoxCircle";
            this.picBoxCircle.Size = new System.Drawing.Size(50, 50);
            this.picBoxCircle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxCircle.TabIndex = 27;
            this.picBoxCircle.TabStop = false;
            this.picBoxCircle.Click += new System.EventHandler(this.picBoxCircle_Click);
            // 
            // picBoxCross
            // 
            this.picBoxCross.Image = ((System.Drawing.Image)(resources.GetObject("picBoxCross.Image")));
            this.picBoxCross.Location = new System.Drawing.Point(15, 16);
            this.picBoxCross.Name = "picBoxCross";
            this.picBoxCross.Size = new System.Drawing.Size(50, 50);
            this.picBoxCross.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxCross.TabIndex = 26;
            this.picBoxCross.TabStop = false;
            this.picBoxCross.Click += new System.EventHandler(this.picBoxCross_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(15, 72);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(106, 23);
            this.btnReset.TabIndex = 25;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // picBox22
            // 
            this.picBox22.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox22.Location = new System.Drawing.Point(338, 188);
            this.picBox22.Name = "picBox22";
            this.picBox22.Size = new System.Drawing.Size(80, 80);
            this.picBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox22.TabIndex = 24;
            this.picBox22.TabStop = false;
            this.picBox22.Click += new System.EventHandler(this.picBox22_Click);
            // 
            // picBox21
            // 
            this.picBox21.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox21.Location = new System.Drawing.Point(252, 188);
            this.picBox21.Name = "picBox21";
            this.picBox21.Size = new System.Drawing.Size(80, 80);
            this.picBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox21.TabIndex = 23;
            this.picBox21.TabStop = false;
            this.picBox21.Click += new System.EventHandler(this.picBox21_Click);
            // 
            // picBox20
            // 
            this.picBox20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox20.Location = new System.Drawing.Point(166, 188);
            this.picBox20.Name = "picBox20";
            this.picBox20.Size = new System.Drawing.Size(80, 80);
            this.picBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox20.TabIndex = 22;
            this.picBox20.TabStop = false;
            this.picBox20.Click += new System.EventHandler(this.picBox20_Click);
            // 
            // picBox12
            // 
            this.picBox12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox12.Location = new System.Drawing.Point(338, 102);
            this.picBox12.Name = "picBox12";
            this.picBox12.Size = new System.Drawing.Size(80, 80);
            this.picBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox12.TabIndex = 21;
            this.picBox12.TabStop = false;
            this.picBox12.Click += new System.EventHandler(this.picBox12_Click);
            // 
            // picBox11
            // 
            this.picBox11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox11.Location = new System.Drawing.Point(252, 102);
            this.picBox11.Name = "picBox11";
            this.picBox11.Size = new System.Drawing.Size(80, 80);
            this.picBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox11.TabIndex = 20;
            this.picBox11.TabStop = false;
            this.picBox11.Click += new System.EventHandler(this.picBox11_Click);
            // 
            // picBox10
            // 
            this.picBox10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox10.Location = new System.Drawing.Point(166, 102);
            this.picBox10.Name = "picBox10";
            this.picBox10.Size = new System.Drawing.Size(80, 80);
            this.picBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox10.TabIndex = 19;
            this.picBox10.TabStop = false;
            this.picBox10.Click += new System.EventHandler(this.picBox10_Click);
            // 
            // picBox02
            // 
            this.picBox02.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox02.Location = new System.Drawing.Point(338, 16);
            this.picBox02.Name = "picBox02";
            this.picBox02.Size = new System.Drawing.Size(80, 80);
            this.picBox02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox02.TabIndex = 18;
            this.picBox02.TabStop = false;
            this.picBox02.Click += new System.EventHandler(this.picBox02_Click);
            // 
            // picBox01
            // 
            this.picBox01.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox01.Location = new System.Drawing.Point(252, 16);
            this.picBox01.Name = "picBox01";
            this.picBox01.Size = new System.Drawing.Size(80, 80);
            this.picBox01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox01.TabIndex = 17;
            this.picBox01.TabStop = false;
            this.picBox01.Click += new System.EventHandler(this.picBox01_Click);
            // 
            // picBox00
            // 
            this.picBox00.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox00.Location = new System.Drawing.Point(166, 16);
            this.picBox00.Name = "picBox00";
            this.picBox00.Size = new System.Drawing.Size(80, 80);
            this.picBox00.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox00.TabIndex = 16;
            this.picBox00.TabStop = false;
            this.picBox00.Click += new System.EventHandler(this.picBox00_Click);
            // 
            // btnPuntaje
            // 
            this.btnPuntaje.Location = new System.Drawing.Point(450, 244);
            this.btnPuntaje.Name = "btnPuntaje";
            this.btnPuntaje.Size = new System.Drawing.Size(145, 23);
            this.btnPuntaje.TabIndex = 28;
            this.btnPuntaje.Text = "Mostrar Puntaje";
            this.btnPuntaje.UseVisualStyleBackColor = true;
            this.btnPuntaje.Click += new System.EventHandler(this.btnPuntaje_Click);
            // 
            // lstBoxPuntajes
            // 
            this.lstBoxPuntajes.FormattingEnabled = true;
            this.lstBoxPuntajes.Location = new System.Drawing.Point(450, 20);
            this.lstBoxPuntajes.Name = "lstBoxPuntajes";
            this.lstBoxPuntajes.Size = new System.Drawing.Size(144, 212);
            this.lstBoxPuntajes.TabIndex = 29;
            // 
            // lstboxTablero
            // 
            this.lstboxTablero.FormattingEnabled = true;
            this.lstboxTablero.Location = new System.Drawing.Point(611, 56);
            this.lstboxTablero.Name = "lstboxTablero";
            this.lstboxTablero.Size = new System.Drawing.Size(144, 212);
            this.lstboxTablero.TabIndex = 30;
            // 
            // btnTablero
            // 
            this.btnTablero.Location = new System.Drawing.Point(611, 20);
            this.btnTablero.Name = "btnTablero";
            this.btnTablero.Size = new System.Drawing.Size(145, 23);
            this.btnTablero.TabIndex = 31;
            this.btnTablero.Text = "Mostrar Tablero";
            this.btnTablero.UseVisualStyleBackColor = true;
            this.btnTablero.Click += new System.EventHandler(this.btnTablero_Click);
            // 
            // HardMode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 297);
            this.Controls.Add(this.btnTablero);
            this.Controls.Add(this.lstboxTablero);
            this.Controls.Add(this.lstBoxPuntajes);
            this.Controls.Add(this.btnPuntaje);
            this.Controls.Add(this.picBoxCircle);
            this.Controls.Add(this.picBoxCross);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.picBox22);
            this.Controls.Add(this.picBox21);
            this.Controls.Add(this.picBox20);
            this.Controls.Add(this.picBox12);
            this.Controls.Add(this.picBox11);
            this.Controls.Add(this.picBox10);
            this.Controls.Add(this.picBox02);
            this.Controls.Add(this.picBox01);
            this.Controls.Add(this.picBox00);
            this.Name = "HardMode";
            this.Text = "HardMode";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCircle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCross)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox00)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxCircle;
        private System.Windows.Forms.PictureBox picBoxCross;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.PictureBox picBox22;
        private System.Windows.Forms.PictureBox picBox21;
        private System.Windows.Forms.PictureBox picBox20;
        private System.Windows.Forms.PictureBox picBox12;
        private System.Windows.Forms.PictureBox picBox11;
        private System.Windows.Forms.PictureBox picBox10;
        private System.Windows.Forms.PictureBox picBox02;
        private System.Windows.Forms.PictureBox picBox01;
        private System.Windows.Forms.PictureBox picBox00;
        private System.Windows.Forms.Button btnPuntaje;
        private System.Windows.Forms.ListBox lstBoxPuntajes;
        private System.Windows.Forms.ListBox lstboxTablero;
        private System.Windows.Forms.Button btnTablero;
    }
}