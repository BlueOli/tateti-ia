﻿
namespace TaTeTi___IA___Forms
{
    partial class MediumMode
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MediumMode));
            this.picBox00 = new System.Windows.Forms.PictureBox();
            this.picBox01 = new System.Windows.Forms.PictureBox();
            this.picBox02 = new System.Windows.Forms.PictureBox();
            this.picBox12 = new System.Windows.Forms.PictureBox();
            this.picBox11 = new System.Windows.Forms.PictureBox();
            this.picBox10 = new System.Windows.Forms.PictureBox();
            this.picBox22 = new System.Windows.Forms.PictureBox();
            this.picBox21 = new System.Windows.Forms.PictureBox();
            this.picBox20 = new System.Windows.Forms.PictureBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.picBoxCross = new System.Windows.Forms.PictureBox();
            this.picBoxCircle = new System.Windows.Forms.PictureBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.lblAntes = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBox00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCross)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCircle)).BeginInit();
            this.SuspendLayout();
            // 
            // picBox00
            // 
            this.picBox00.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox00.Location = new System.Drawing.Point(182, 27);
            this.picBox00.Name = "picBox00";
            this.picBox00.Size = new System.Drawing.Size(80, 80);
            this.picBox00.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox00.TabIndex = 0;
            this.picBox00.TabStop = false;
            this.picBox00.Click += new System.EventHandler(this.picBox00_Click);
            // 
            // picBox01
            // 
            this.picBox01.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox01.Location = new System.Drawing.Point(268, 27);
            this.picBox01.Name = "picBox01";
            this.picBox01.Size = new System.Drawing.Size(80, 80);
            this.picBox01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox01.TabIndex = 1;
            this.picBox01.TabStop = false;
            this.picBox01.Click += new System.EventHandler(this.picBox01_Click);
            // 
            // picBox02
            // 
            this.picBox02.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox02.Location = new System.Drawing.Point(354, 27);
            this.picBox02.Name = "picBox02";
            this.picBox02.Size = new System.Drawing.Size(80, 80);
            this.picBox02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox02.TabIndex = 2;
            this.picBox02.TabStop = false;
            this.picBox02.Click += new System.EventHandler(this.picBox02_Click);
            // 
            // picBox12
            // 
            this.picBox12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox12.Location = new System.Drawing.Point(354, 113);
            this.picBox12.Name = "picBox12";
            this.picBox12.Size = new System.Drawing.Size(80, 80);
            this.picBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox12.TabIndex = 5;
            this.picBox12.TabStop = false;
            this.picBox12.Click += new System.EventHandler(this.picBox12_Click);
            // 
            // picBox11
            // 
            this.picBox11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox11.Location = new System.Drawing.Point(268, 113);
            this.picBox11.Name = "picBox11";
            this.picBox11.Size = new System.Drawing.Size(80, 80);
            this.picBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox11.TabIndex = 4;
            this.picBox11.TabStop = false;
            this.picBox11.Click += new System.EventHandler(this.picBox11_Click);
            // 
            // picBox10
            // 
            this.picBox10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox10.Location = new System.Drawing.Point(182, 113);
            this.picBox10.Name = "picBox10";
            this.picBox10.Size = new System.Drawing.Size(80, 80);
            this.picBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox10.TabIndex = 3;
            this.picBox10.TabStop = false;
            this.picBox10.Click += new System.EventHandler(this.picBox10_Click);
            // 
            // picBox22
            // 
            this.picBox22.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox22.Location = new System.Drawing.Point(354, 199);
            this.picBox22.Name = "picBox22";
            this.picBox22.Size = new System.Drawing.Size(80, 80);
            this.picBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox22.TabIndex = 8;
            this.picBox22.TabStop = false;
            this.picBox22.Click += new System.EventHandler(this.picBox22_Click);
            // 
            // picBox21
            // 
            this.picBox21.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox21.Location = new System.Drawing.Point(268, 199);
            this.picBox21.Name = "picBox21";
            this.picBox21.Size = new System.Drawing.Size(80, 80);
            this.picBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox21.TabIndex = 7;
            this.picBox21.TabStop = false;
            this.picBox21.Click += new System.EventHandler(this.picBox21_Click);
            // 
            // picBox20
            // 
            this.picBox20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.picBox20.Location = new System.Drawing.Point(182, 199);
            this.picBox20.Name = "picBox20";
            this.picBox20.Size = new System.Drawing.Size(80, 80);
            this.picBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox20.TabIndex = 6;
            this.picBox20.TabStop = false;
            this.picBox20.Click += new System.EventHandler(this.picBox20_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(469, 54);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(180, 225);
            this.listBox1.TabIndex = 9;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(31, 83);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(106, 23);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // picBoxCross
            // 
            this.picBoxCross.Image = ((System.Drawing.Image)(resources.GetObject("picBoxCross.Image")));
            this.picBoxCross.Location = new System.Drawing.Point(31, 27);
            this.picBoxCross.Name = "picBoxCross";
            this.picBoxCross.Size = new System.Drawing.Size(50, 50);
            this.picBoxCross.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxCross.TabIndex = 14;
            this.picBoxCross.TabStop = false;
            this.picBoxCross.Click += new System.EventHandler(this.picBoxCross_Click);
            // 
            // picBoxCircle
            // 
            this.picBoxCircle.Image = global::TaTeTi___IA___Forms.Properties.Resources.Circle;
            this.picBoxCircle.Location = new System.Drawing.Point(87, 27);
            this.picBoxCircle.Name = "picBoxCircle";
            this.picBoxCircle.Size = new System.Drawing.Size(50, 50);
            this.picBoxCircle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxCircle.TabIndex = 15;
            this.picBoxCircle.TabStop = false;
            this.picBoxCircle.Click += new System.EventHandler(this.picBoxCircle_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(672, 54);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(180, 225);
            this.listBox2.TabIndex = 16;
            // 
            // lblAntes
            // 
            this.lblAntes.AutoSize = true;
            this.lblAntes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAntes.Location = new System.Drawing.Point(668, 27);
            this.lblAntes.Name = "lblAntes";
            this.lblAntes.Size = new System.Drawing.Size(61, 20);
            this.lblAntes.TabIndex = 17;
            this.lblAntes.Text = "Antes:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(465, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Despues:";
            // 
            // MediumMode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 314);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAntes);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.picBoxCircle);
            this.Controls.Add(this.picBoxCross);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.picBox22);
            this.Controls.Add(this.picBox21);
            this.Controls.Add(this.picBox20);
            this.Controls.Add(this.picBox12);
            this.Controls.Add(this.picBox11);
            this.Controls.Add(this.picBox10);
            this.Controls.Add(this.picBox02);
            this.Controls.Add(this.picBox01);
            this.Controls.Add(this.picBox00);
            this.Name = "MediumMode";
            this.Text = "Medium Mode";
            ((System.ComponentModel.ISupportInitialize)(this.picBox00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCross)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCircle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBox00;
        private System.Windows.Forms.PictureBox picBox01;
        private System.Windows.Forms.PictureBox picBox02;
        private System.Windows.Forms.PictureBox picBox12;
        private System.Windows.Forms.PictureBox picBox11;
        private System.Windows.Forms.PictureBox picBox10;
        private System.Windows.Forms.PictureBox picBox22;
        private System.Windows.Forms.PictureBox picBox21;
        private System.Windows.Forms.PictureBox picBox20;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.PictureBox picBoxCross;
        private System.Windows.Forms.PictureBox picBoxCircle;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label lblAntes;
        private System.Windows.Forms.Label label1;
    }
}

