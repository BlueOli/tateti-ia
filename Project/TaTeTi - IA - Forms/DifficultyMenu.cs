﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaTeTi___IA___Forms
{
    public partial class DifficultyMenu : Form
    {
        private EasyMode easyForm;
        private MediumMode mediumForm;
        private HardMode hardForm;

        public DifficultyMenu()
        {
            InitializeComponent();
        }

        private void btnEasy_Click(object sender, EventArgs e)
        {
            easyForm = new EasyMode();
            easyForm.Show();
            this.Hide();
        }

        private void btnMedium_Click(object sender, EventArgs e)
        {
            mediumForm = new MediumMode();
            mediumForm.Show();
            this.Hide();
        }

        private void btnHard_Click(object sender, EventArgs e)
        {
            hardForm = new HardMode();
            hardForm.Show();
            this.Hide();
        }
    }
}
